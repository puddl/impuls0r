from django.apps import AppConfig


class Impuls0rConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'impuls0r'
