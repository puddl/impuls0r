import django.forms
from django.db import models as djm
from django.contrib import admin

from . import models


@admin.register(models.User)
class UserAdmin(admin.ModelAdmin):
    # displays postgres textfield as simple <input type="text"/> instead of <textarea/>
    formfield_overrides = {
        djm.TextField: {'widget': django.forms.TextInput},
    }
