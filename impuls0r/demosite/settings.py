"""
https://docs.djangoproject.com/en/3.2/topics/settings/
https://docs.djangoproject.com/en/3.2/ref/settings/
https://docs.djangoproject.com/en/3.2/howto/deployment/checklist/
"""
import os

import dj_database_url

BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))

# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = os.environ.get('SECRET_KEY')

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = os.environ.get('DEBUG', 'false').lower() == 'true'

EMAIL_BACKEND = 'django.core.mail.backends.console.EmailBackend'

ALLOWED_HOSTS = ['*']

INSTALLED_APPS = [
    'impuls0r',  # must come before auth, so template overrides actually override stuff
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.humanize',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.sites',
    'django.contrib.staticfiles',
    'django_extensions',
]
SITE_ID = 1

MIDDLEWARE = [
    'django.middleware.security.SecurityMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
]

ROOT_URLCONF = 'impuls0r.demosite.urls'

TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'DIRS': [],
        'APP_DIRS': True,
        'OPTIONS': {
            'context_processors': [
                'django.template.context_processors.debug',
                'django.template.context_processors.media',
                'django.template.context_processors.request',
                'django.contrib.auth.context_processors.auth',
                'django.contrib.messages.context_processors.messages',
            ],
        },
    },
]

WSGI_APPLICATION = 'impuls0r.demosite.wsgi.application'

# https://docs.djangoproject.com/en/3.2/ref/settings/#databases
DATABASES = {'default': dj_database_url.config(env='DB_URL')}

_default_log_level = 'DEBUG' if DEBUG else 'INFO'
LOG_LEVEL = os.environ.get('LOGLEVEL', _default_log_level).upper()
LOG_SQL = os.environ.get('LOG_SQL', 'false').lower() == 'true'
LOGGING = {
    'version': 1,
    'disable_existing_loggers': False,
    'formatters': {
        # https://docs.python.org/3/library/logging.html#logrecord-attributes
        'simple': {'format': '[%(asctime)s] %(levelname)s %(name)s %(message)s', 'datefmt': "%Y-%m-%d %H:%M:%S"},
    },
    'handlers': {
        'console': {'level': LOG_LEVEL, 'class': 'logging.StreamHandler', 'formatter': 'simple'},
    },
    # add custom loggers here
    'loggers': {
        # https://stackoverflow.com/a/31251707
        'app': {
            'handlers': ['console'],
            'level': LOG_LEVEL,
            'propagate': False,
        },
        'django': {
            'handlers': ['console'],
            'level': 'INFO',
            'propagate': True,
        },
    },
}
if LOG_SQL:
    LOGGING['loggers']['django.db.backends'] = {
        'handlers': ['console'],
        'level': 'DEBUG',
    }

# https://docs.djangoproject.com/en/3.2/ref/settings/#auth-password-validators
AUTH_PASSWORD_VALIDATORS = [
    {
        'NAME': 'django.contrib.auth.password_validation.UserAttributeSimilarityValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.MinimumLengthValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.CommonPasswordValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.NumericPasswordValidator',
    },
]

# https://docs.djangoproject.com/en/3.2/topics/i18n/
LANGUAGE_CODE = 'en-us'
TIME_ZONE = 'UTC'
USE_I18N = True
USE_L10N = True
USE_TZ = True

# https://docs.djangoproject.com/en/3.2/howto/static-files/
STATIC_URL = '/static/'
STATIC_ROOT = os.environ['STATIC_ROOT']
MEDIA_URL = '/media/'
MEDIA_ROOT = os.environ['MEDIA_ROOT']

# https://docs.djangoproject.com/en/3.2/ref/settings/#std:setting-STATICFILES_FINDERS
STATICFILES_FINDERS = [
    'django.contrib.staticfiles.finders.FileSystemFinder',
    'django.contrib.staticfiles.finders.AppDirectoriesFinder',
]

AUTH_USER_MODEL = 'impuls0r.User'
# this only comes into play with TLS
# https://docs.djangoproject.com/en/3.2/ref/settings/#csrf-trusted-origins
CSRF_TRUSTED_ORIGINS = os.environ.get('CSRF_TRUSTED_ORIGINS', '').split(',')
_cookie_domain = os.environ.get('COOKIE_DOMAIN', '')
SESSION_COOKIE_DOMAIN = _cookie_domain
# https://docs.djangoproject.com/en/3.2/ref/csrf/#csrf-limitations
CSRF_COOKIE_DOMAIN = _cookie_domain
LANGUAGE_COOKIE_DOMAIN = _cookie_domain
CSRF_COOKIE_SECURE = True

EMAIL_HOST = os.environ.get('SMTP_HOST')
EMAIL_HOST_PASSWORD = os.environ.get('SMTP_PASSWORD')
EMAIL_HOST_USER = os.environ.get('SMTP_USERNAME')
EMAIL_PORT = os.environ.get('SMTP_PORT')
EMAIL_USE_SSL = True
DEFAULT_FROM_EMAIL = os.environ.get('SMTP_FROM_ADDRESS')
