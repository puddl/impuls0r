This is based on https://gitlab.com/puddl/template-django-app


# Development
Create a virtualenv and install
```
pyenv virtualenv 3.9.5 impuls0r
pyenv local impuls0r
pip install -U pip

pip install -e .[dev]
```

Create a database
```
puddl db create impuls0r
db_url=$(puddl db url impuls0r)
# postgresql://impuls0r:***@127.0.0.1:13370/puddl?application_name=impuls0r
```

Configure and start
```
cat <<EOF > .env
SECRET_KEY=change-me
STATIC_ROOT=env/dev/static_root/
MEDIA_ROOT=env/dev/media_root/

DB_URL=${db_url}
EOF

pytest

./manage.py makemigrations
./manage.py migrate
./manage.py runserver
```

See the following for more environment variables:
```
grep os.environ impuls0r/demosite/settings.py
```


# Dependencies and their reasons
- `dj-database-url` because it's by jacobian
- `django_extensions` for `./manage.py shell_plus --print-sql`
- `psycopg2-binary` because PostgreSQL is the best
- `python-dotenv` because `.env` is nice
